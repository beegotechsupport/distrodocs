This file describes some of the tools found on the Beegux desktop, and their purpose.

Disks:
Also known as gnome-disks or gnome-disk-utlity,
mostly useful for checking the SMART status of a hard drive.
This is a first stop if there are boot or speed issues.
Also lists partitions and offers formatting tools.

TestDisk:
Restores partitions or deleted files.
Only worth trying when caused by a software issue/user error
Full guide here: -Fill in later-
